#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <time.h>
#include <sys/wait.h>
#include <string.h>
#include <stdbool.h>

int main(int argc, char *argv[])
{

int hour, minute, second;

if (argc < 5 || argc > 5) {
    printf("invalid argument: %s <hour> <minute> <second> <script-path>\n", argv[0]);
    return 1;
}//revisi pada bagian ini

if (strcmp(argv[1], "*") == 0) {
    hour = -1;
} else {
    hour = atoi(argv[1]);
    if (hour < 0 || hour > 23) {
        printf("Invalid hour value\n");
        return 1;
    }
}

if (strcmp(argv[2], "*") == 0) {
    minute = -1;
} else {
    minute = atoi(argv[2]);
    if (minute < 0 || minute > 59) {
        printf("Invalid minute value\n");
        return 1;
    }
}

if (strcmp(argv[3], "*") == 0) {
    second = -1;
} else {
    second = atoi(argv[3]);
    if (second < 0 || second > 59) {
        printf("Invalid second value\n");
        return 1;
    }
}

    // Define the arguments for the program to be executed
    char *args[] = {"/bin/bash", argv[4], NULL};

    // getting time
    time_t now = time(NULL);
    struct tm *timeStruct = localtime(&now);
    int hournow, minnow, secnow;
    bool first = true;
    
    hournow = timeStruct->tm_hour;
    minnow = timeStruct->tm_min;
    secnow = timeStruct->tm_sec;

    int sleeping = 0;

    if (hour == -1)
    {
        if (minute == -1)
        {
            if (second >= 0)
            {

                if (second < secnow)
                {
                    sleeping = (second + 60) - secnow;
                }

                else
                {
                    sleeping = second - secnow;
                }
            }
            else
            {
                sleeping = 0;
            }
        }
        else
        {
            sleeping = (((minute * 60) + second) - ((minnow * 60) + secnow));
            if (minute < minnow)
            {
                sleeping += 3600;
            }
            else if (minute == minnow)
            {
                sleeping = 0;
            }
        }
    }

    else
    {
        sleeping = (((hour * 3600) + (minute * 60) + second) - ((hournow * 3600) + (minnow * 60) + secnow));
        if (hour < hournow)
        {
            sleeping += 86400;
        }
        else if (hour == hournow)
        {
            sleeping = 0;
        }
    }

    printf(" current hour: %d %d %d \n", hournow, minnow, secnow);
    printf("cronjob hour : %d %d %d \n", hour, minute, second);
    printf("%d\n", sleeping);

    // Fork a new process
    pid_t pid = fork();

    if (pid == -1)
    {
        printf("Failed to fork process\n");
        return 1;
    }
    else if (pid == 0)
    {
        // In child process
        // Run the specified program at the specified interval

                        
        while (1)
        {
            
            int slept = 0;
            if(hour == -1 && minute == -1 && second == -1){
                slept = 1;
            }
            
            if (hour != -1)
            {
                slept += 86400;
            }
            else if (minute != -1)
            {
                slept += 3600;
            }
            else if (second != -1)
            {
                slept += 60;
            }

            if (first == true)
            {
                sleep(sleeping);
                first = false;

                if (hour ==-1)
                {

                    if (minute == -1)
                    {

                        if (second == -1)
                        {
                            slept = 1;
                        }
                    }
                    else
                    {
                        if (second == -1)
                        {
                            slept -= secnow;
                        }
                    }
                }
                else
                {
                    if (minute == -1 && second == -1)
                    {
                        slept -= ((minnow * 60) + secnow);
                    }
                    else if (minute == -1 && second >= 0)
                    {
                        slept -= (minnow * 60);
                    }
                    else if(minute >= 0 && second == -1)
                    {
                        slept -= secnow;
                    }
                }
            }

            pid_t exec_pid = fork();
            if (exec_pid == -1)
            {
                printf("Failed to fork process\n");
            }

            else if (exec_pid == 0)
            {
                // Child process for executing the script
                execvp(args[0], args);
            }
            else
            {
                // Parent process waits for the script to finish execution
                wait(NULL);
            }

            // print slept interval
            printf("slept time: %d\n", slept);
            sleep(slept);
        }
    }
    else
    {
        //ini parent yang di exit, supaya program berjalan di background
        printf("Timer process is running in the background with PID: %d\n", pid);
        return 0;
    }
}
