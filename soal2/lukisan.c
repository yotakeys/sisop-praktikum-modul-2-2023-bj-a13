#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <string.h>

void create_killer(pid_t pid, char argmodes){
    pid_t child_id;
    int status;

    char current_directory[120] = "";
    realpath(".", current_directory);

    child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        char path_program_killer_c[120];
        int max_len_path_program_killer_c;
        char path_killer[120];
        int max_len_path_path_killer;
        char program_killer_c[1200];
        char argmode[120];

        snprintf(path_program_killer_c, max_len_path_program_killer_c, "%s/killer.c", current_directory);
        snprintf(path_killer, max_len_path_path_killer, "%s/killer", current_directory);

        if (argmodes == 'a') 
          strcpy(argmode, " char *argv[] = {\"killall\", \"-9\", \"lukisan\", NULL}; execv(\"/usr/bin/killall\", argv);");
        else if (argmodes == 'b') 
          sprintf(argmode, " char *argv[] = {\"kill\", \"%d\", NULL}; execv(\"/usr/bin/kill\", argv);", pid);
        
        sprintf(program_killer_c,   "#include <stdio.h>\n"
                                    "#include <stdlib.h>\n"
                                    "#include <unistd.h>\n"
                                    "#include <sys/types.h>\n"
                                    "#include <wait.h>\n"
                                    "#include <string.h>\n"

                                    "int main() {"
                                        "pid_t child_id;"
                                        "int status;"

                                        "child_id = fork();"
                                        "if (child_id < 0) exit(EXIT_FAILURE);"

                                        "if (child_id == 0) {"
                                            " %s"
                                        "} else {"
                                            " while (wait(&status)>0);"
                                            " char *argremove[] = {\"rm\", \"%s/killer\", NULL};"
                                            " execv(\"/bin/rm\", argremove);"
                                        "}"
                                    "}", argmode, current_directory);

        FILE *file_killer;
        file_killer = fopen(path_program_killer_c, "w");
        fputs(program_killer_c, file_killer);
        fclose(file_killer);

        char *argv[] = {"gcc", path_program_killer_c, "-o", path_killer, NULL};
        execv("/usr/bin/gcc", argv);
    }
    
    wait(&status);

    child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        char path_program_killer_c[120];
        int max_len_path_program_killer_c;
        snprintf(path_program_killer_c, max_len_path_program_killer_c, "%s/killer.c", current_directory);

        char *argremove[] = {"rm", path_program_killer_c, NULL};
        execv("/bin/rm", argremove);
    }
}

int main(int argc, char *argv[]) {
     if(argc != 2 || argv[1][1] != 'a' && argv[1][1] != 'b'){
        printf("Jalankan program dengan memilih paramter sebagai berikut:\n -a (./lukisan -a) atau -b (./lukisan -b)\n");
        printf("\n Berikut detail penjelesan program dengan parameter yang digunakan\n");
        printf("- Paramter -a :\n ketika program killer dijalankan, program utama akan langsung menghentikan semua operasinya\n");
        printf("\n- Paramter -b :\n ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete)\n");
        return 0;
    }

    pid_t pid, sid;
    
    pid = fork();

    if (pid < 0) {
      exit(EXIT_FAILURE);
    }

    if (pid > 0) {
      exit(EXIT_SUCCESS);
    }

    create_killer(getpid(), argv[1][1]);

    umask(0);

    sid = setsid();
    if (sid < 0) {
      exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while(1) {
        pid_t child_id;
        int status;

        char folder_name[72];
        char path_folder_name[72];
        time_t current_time;
        struct tm *time_info;
        time(&current_time);
        time_info = localtime(&current_time);
        strftime(folder_name, 72, "%Y-%m-%d_%H:%M:%S", time_info);
        strcpy(path_folder_name, "./");
        strcat(path_folder_name, folder_name);
        
        char *argsfolder[] = {"mkdir", "-p", path_folder_name, NULL};

        child_id = fork();
        if (child_id < 0) {
            exit(EXIT_FAILURE);
        }
        if (child_id == 0) { 
            execv("/bin/mkdir", argsfolder);
        }
        else {
            wait(&status); 
        }

        child_id = fork();
        if (child_id < 0) {
            exit(EXIT_FAILURE); 
        }
        if (child_id == 0) {
            for (int i = 0; i < 15; i++) {
                time_t current_time;
                struct tm *time_info;
                char image_name[72];
                char path_file_name[72];
                char url[72];

                time(&current_time);
                time_info = localtime(&current_time);
                strftime(image_name, 72, "%Y-%m-%d_%H:%M:%S", time_info);

                strcpy(path_file_name, "./");
                strcat(path_file_name, folder_name);
                strcat(path_file_name, "/");
                strcat(path_file_name, image_name);

                int size = (current_time % 1000) + 50;
                sprintf(url, "https://picsum.photos/%d", size);

                char* argsdownload[] = {"wget", "-q", "-O", path_file_name, url, NULL};

                child_id = fork();
                if (child_id < 0) {
                    exit(EXIT_FAILURE);
                }
                if (child_id == 0) { 
                    execv("/usr/bin/wget", argsdownload);
                }
                else {
                    wait(&status); 
                }
                sleep(5);
            }
            
            char *argszip[] = {"zip", "-r", path_folder_name, path_folder_name, NULL};
            char *agrsremove[]= { "rm", "-r", path_folder_name, NULL};

            child_id = fork();
            if (child_id < 0) {
                exit(EXIT_FAILURE);
            }
            if (child_id == 0) { 
                execv("/usr/bin/zip", argszip);
            }
            else {
                wait(&status); 
            }

            child_id = fork();
            if (child_id < 0) {
                exit(EXIT_FAILURE);
            }
            if (child_id == 0){ 
                execv("/bin/rm", agrsremove);
            }
            else {
                wait(&status); 
            }
            exit(0);
        }
        else {
            sleep(30);
        }
    }
}