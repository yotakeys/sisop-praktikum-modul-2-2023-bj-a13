# Sisop Praktikum Modul 2 2023 BJ-A13

Anggota : 
1. Keyisa Raihan I. S.  (5025211002)
2. M. Taslam Gustino    (5025211011)
3. Adrian Karuna S.     (5025211019)
4. Vito Febrian Ananta  (5025211224)

## Soal 1
Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari drive kebun binatang tersebut : https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq

a. Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.

b. Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.

c. Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.

d. Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

Catatan : 
- Untuk melakukan zip dan unzip tidak boleh menggunakan system

Penyelesaian dan penjelasan:
#Bagian A
```c
  pid_t child_id;
  int status;

  //download in link
  child_id = fork();
  if(child_id < 0){
    exit(EXIT_FAILURE);
  }else if(child_id == 0){ //child process
    char *argv[] = {"wget", "-O", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "&", NULL};
    execv("/usr/bin/wget", argv);
    return 0;
  }
  
  while((wait(&status)) > 0);

  //unzip the zip file
  child_id = fork();
  if(child_id < 0){
    exit(EXIT_FAILURE);
  }else if(child_id == 0){ //child process
    char *argv[] = {"unzip", "binatang.zip", NULL};
    execv("/usr/bin/unzip", argv);
    return 0;
  }
```

- Kita mendownload file binatang.zip pada link yang sudah diberikan soal dengan cara melakukan fork untuk membuat child proses
- Karena untuk unzip file yang akan didownload harus menyelesaikan download file terlebih dahulu, maka dibuat jeda sampai download file selesai
- Kemudian melakukan unzip pada file yang sudah didownload tersebut dengan membuat child proses juga

#Bagian B
```c
while((wait(&status)) > 0);

  //take random filename
  child_id = fork();
  if(child_id < 0){
    exit(EXIT_FAILURE);
  }else if(child_id == 0){ //child process
    struct dirent *entry;
    DIR *dir = opendir(".");
    if(dir == NULL){
        printf("Error open dir");
        exit(EXIT_FAILURE);
    }

    //count number of file
    int count = 0;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            count++;
        }
    }
    
    //allocate memory for name of file
    char **files = malloc(count * sizeof(char *));
    if (files == NULL) {
        printf("Error malloc");
        exit(EXIT_FAILURE);
    }

    //return the pointer back to first file
    rewinddir(dir);
    
    //take name of file name
    int i = 0;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG && strstr(entry->d_name, ".jpg")) { 
            files[i] = entry->d_name;
            i++;
        }
    }
    //close directory
    closedir(dir);

    //random number
    srand(time(NULL));
    int random = rand() % i;
    
    //select file
    char filename[50];
    strcpy(filename, files[random]);
    printf("==========================================================\n");
    printf("MELAKUKAN SHIFT PENJAGAAN PADA : %s\n", filename);
    printf("==========================================================\n");
    free(files);
    
    return 0;
  }
  //close directory
  closedir(dir);
  
  //random number
  srand(time(NULL));
  int random = rand() % i;
    
  //select file
  char filename[50];
  strcpy(filename, files[random]);
  printf("==========================================================\n");
  printf("MELAKUKAN SHIFT PENJAGAAN PADA : %s\n", filename);
  printf("==========================================================\n");
  free(files);
  
  return 0;
}
```

- Membuat child proses dengan fork
- Membuka directory yang sesuai
- Membaca directory untuk menghitung jumlah file yang ada di directory tersebut. Hal tersebut digunakan untuk memperkirakan alokasi memori untuk jumlah file. Setelah itu mengembalikan pointer keposisi semula
- Mengambil nama dari file yang ada di directory, tetapi yang dimasukkan kedalam array of string hanya yang bertipekan jpg saja. Jadi file zip yang didownload ataupun script ini tidak ikut dimasukkan kedalam array files
- Selanjutnya sembari mengambil nama file, juga dihitung jumlah file .jpg untuk keperluan mencari nilai acak. Kemudian tutup directory
- Untuk menentukan nama file acak yang ada didalam array maka dibutuhkan angka acak yang dimodulo dengan panjang array untuk mendapatkan nilai yang sesuai dengan panjang arraynya
- Kemudian ditampilkan kedalam console nama file tersebut

#Bagian C
```c
  while((wait(&status)) > 0);

  //create directory
  system("mkdir HewanDarat HewanAir HewanAmphibi");
  //move file
  system("mv *amphibi.jpg HewanAmphibi");
  system("mv *darat.jpg HewanDarat");
  system("mv *air.jpg HewanAir");
```

- Untuk memilah file kedalam directory yang ditentukan maka diperlukan waktu jeda sampai proses membaca file selesai, baru kemudian dilakukan pemilahan
- Membuat directory sesuai yang diinginkan soal
- Memindahkan atau memilah file sesuai dengan namanya menurut nama directory-nya

#Bagian D
```c
  while((wait(&status)) > 0);

  //zip the directory
  child_id = fork();
  if(child_id < 0){
    exit(EXIT_FAILURE);
  }else if(child_id == 0){ //child process
    char *argv[] = {"zip", "-r", "HewanAir.zip", "HewanAir", NULL};
    execv("/usr/bin/zip", argv);
    return 0;
  }
  child_id = fork();
  if(child_id < 0){
    exit(EXIT_FAILURE);
  }else if(child_id == 0){ //child process
    char *argv[] = {"zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
    execv("/usr/bin/zip", argv);
    return 0;
  }
  child_id = fork();
  if(child_id < 0){
    exit(EXIT_FAILURE);
  }else if(child_id == 0){ //child process
    char *argv[] = {"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
    execv("/usr/bin/zip", argv);
    return 0;
  }
  
  //delete all directory just leaving zip file
  while((wait(&status)) > 0);
  system("rm -rf HewanAir HewanDarat HewanAmphibi binatang.zip");
  return 0;
```

- Untuk melakukan zip masing-masing directory maka diperlukan waktu jeda sampai semua file selesai dipindahkan
- Membuat child proses sebanyak tiga kali untuk masing-masing proses zip pada directory dengan nama yang sesuai dibutuhkan soal
- Untuk fitur tambahan agar tidak begitu banyak memakan memory maka dilakukan penghapusan file yang awal didownload dan juga directory yang sudah di zip

Output random console
```
==========================================================
MELAKUKAN SHIFT PENJAGAAN PADA : beruang_darat.jpg
==========================================================
```
Output file zip
![Screenshot_from_2023-03-24_21-41-33](/uploads/ce55f56f746f251b55d584b2c329fdbd/Screenshot_from_2023-03-24_21-41-33.png)
## Soal 2
Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !

a. Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].

b. Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].

c. Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).

d. Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.

e. Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

Catatan :
- Tidak boleh menggunakan system()
- Proses berjalan secara daemon
- Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)

Penyelesaian dan penjelasan:

#Poin a
```c
    while (1) {
        pid_t child_id;
        int status;

        char folder_name[72];
        char path_folder_name[72];
        time_t current_time;
        struct tm *time_info;
        time(&current_time);
        time_info = localtime(&current_time);
        strftime(folder_name, 72, "%Y-%m-%d_%H:%M:%S", time_info);
        strcpy(path_folder_name, "./");
        strcat(path_folder_name, folder_name);
        
        char *argsfolder[] = {"mkdir", "-p", path_folder_name, NULL};

        child_id = fork();
        if (child_id < 0) {
            exit(EXIT_FAILURE);
        }
        if (child_id == 0) { 
            execv("/bin/mkdir", argsfolder);
        }
        else {
            wait(&status); 
        }

        child_id = fork();
        if (child_id < 0) {
            exit(EXIT_FAILURE); 
        }
        if (child_id == 0) {
            // program download gambar serta zip dan remove
        }
        else {
          sleep(30);
        }
    }
```
Poin a dapat menyelesaikannya dengan cara sebagai berikut:
- Untuk membuat folder digunakan command mkdir yang diexec menggunakan execv dengan parameter -p untuk mengatasi exists folder serta dengan path_folder_name sebagai path folder akan dibuat. Diguanakn execv, karena tidak boleh menggunakan system().
- Untuk membuat nama folder tersebut menjadi timestamp diambil waktu dengan variabel current_time dengan bantuan library time.h, karena struktur waktunya masih belum sesuai, maka dikonversi menggunakan localtime(). Setelah itu, time_info tersebut diubah format waktu tersebut menjadi bentuk string dengan strftime().
- Untuk terus membuat folder dengan selang 30 detik, maka digunakan while(1) untuk infinite loop dan dilakukan sleep(30).

#Poin b
```c
            for (int i = 0; i < 15; i++) {
                time_t current_time;
                struct tm *time_info;
                char image_name[72];
                char path_file_name[72];
                char url[72];

                time(&current_time);
                time_info = localtime(&current_time);
                strftime(image_name, 72, "%Y-%m-%d_%H:%M:%S", time_info);

                strcpy(path_file_name, "./");
                strcat(path_file_name, folder_name);
                strcat(path_file_name, "/");
                strcat(path_file_name, image_name);

                int size = (current_time % 1000) + 50;
                sprintf(url, "https://picsum.photos/%d", size);

                char* argsdownload[] = {"wget", "-q", "-O", path_file_name, url, NULL};

                child_id = fork();
                if (child_id < 0) {
                    exit(EXIT_FAILURE);
                }
                if (child_id == 0) { 
                    execv("/usr/bin/wget", argsdownload);
                }
                else {
                    wait(&status); 
                }
                sleep(5);
            }
```
Poin b dapat menyelesaikannya dengan cara sebagai berikut:
- Untuk mendownload 15 gambar, digunakan for loop selama 15 kali.
- Untuk mendapatkan ukuran gambar, digunakan pengambilan waktu seperti pada poin a lalu dilakukan kalkulasi (t % 1000) + 50. Setelah itu, dilakukan pembuatan url sesuai ukuran gambar dengan digunakannya sprintf().
- Untuk mendownload gambar dari url tersebut, digunakan commad wget dengan parameter -O untuk mengoutputkan FILE serta -q yang berarti quiet (no output) agar FILE tersebut menjadi gambar.
- Agar proses pembuatan folder dan pengunduhdan gambar dapat berjalan overlapping maka kedua proses dilakukan fork() agar dapat berjalan dengan process yang berbeda dengan wait(&status) karena pengunduhan gambar hanyak akan dilakukan jika ada pembuatan folder.

#Poin c
```c
            char *argszip[] = {"zip", "-r", path_folder_name, path_folder_name, NULL};
            char *agrsremove[]= { "rm", "-r", path_folder_name, NULL};

            child_id = fork();
            if (child_id < 0) {
                exit(EXIT_FAILURE);
            }
            if (child_id == 0) { 
                execv("/usr/bin/zip", argszip);
            }
            else {
                wait(&status); 
            }

            child_id = fork();
            if (child_id < 0) {
                exit(EXIT_FAILURE);
            }
            if (child_id == 0){ 
                execv("/bin/rm", agrsremove);
            }
            else {
                wait(&status); 
            }
            exit(0);
```
Poin c dapat menyelesaikannya dengan cara sebagai berikut:
- Untuk merapikan folder, digunakan command zip dengan parameter -r (rekursif), karena suatu folder akan berisi 15 gambar serta dieksekusi dengan execv(). Setelah itu akan didelete folder tersebut dengan digunankannya command rm dengan parameter -r dengan alasan yang sama seperti zip serta cara eksekusi yang juga sama.
- Untuk menandakan bahwa suatu proses telah selesai dilakukan, maka digunakan exit(0).

#Poin d
```c
void create_killer(pid_t pid, char argmodes){
    pid_t child_id;
    int status;

    char current_directory[120] = "";
    realpath(".", current_directory);

    child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        char path_program_killer_c[120];
        int max_len_path_program_killer_c;
        char path_killer[120];
        int max_len_path_path_killer;
        char program_killer_c[1200];
        char argmode[120];

        snprintf(path_program_killer_c, max_len_path_program_killer_c, "%s/killer.c", current_directory);
        snprintf(path_killer, max_len_path_path_killer, "%s/killer", current_directory);

        if (argmodes == 'a') 
          strcpy(argmode, " char *argv[] = {\"killall\", \"-9\", \"lukisan\", NULL}; execv(\"/usr/bin/killall\", argv);");
        else if (argmodes == 'b') 
          sprintf(argmode, " char *argv[] = {\"kill\", \"%d\", NULL}; execv(\"/usr/bin/kill\", argv);", pid);
        
        sprintf(program_killer_c,   "#include <stdio.h>\n"
                                    "#include <stdlib.h>\n"
                                    "#include <unistd.h>\n"
                                    "#include <sys/types.h>\n"
                                    "#include <wait.h>\n"
                                    "#include <string.h>\n"

                                    "int main() {"
                                        "pid_t child_id;"
                                        "int status;"

                                        "child_id = fork();"
                                        "if (child_id < 0) exit(EXIT_FAILURE);"

                                        "if (child_id == 0) {"
                                            " %s"
                                        "} else {"
                                            " while (wait(&status)>0);"
                                            " char *argremove[] = {\"rm\", \"%s/killer\", NULL};"
                                            " execv(\"/bin/rm\", argremove);"
                                        "}"
                                    "}", argmode, current_directory);

        FILE *file_killer;
        file_killer = fopen(path_program_killer_c, "w");
        fputs(program_killer_c, file_killer);
        fclose(file_killer);

        char *argv[] = {"gcc", path_program_killer_c, "-o", path_killer, NULL};
        execv("/usr/bin/gcc", argv);
    }
    
    wait(&status);

    child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        char path_program_killer_c[120];
        int max_len_path_program_killer_c;
        snprintf(path_program_killer_c, max_len_path_program_killer_c, "%s/killer.c", current_directory);

        char *argremove[] = {"rm", path_program_killer_c, NULL};
        execv("/bin/rm", argremove);
    }
}
```
Poin d dapat menyelesaikannya dengan cara sebagai berikut:
- Untuk membuat program killer, digunakan fungsi create_killer yang dibuat dengan digunakannya sprintf(). Fungsi sprintf tersebut akan diisi dengan progarm c.
- Pada program c tersebut, pertama-tama akan dilakukan fork() untuk membuat child process agar bisa langsung menghapus program killer itu sendiri. Pada parent process akan diisi dengan variabel argmode yang merupakan variabel yang menyimpan sebuah mode argumen yang diterima ketika lukisan.c dijalankan. Setelah itu, pada child process akan menunggu parentnya selesai (exe killer telah dijalankan ) untuk melakukan penghapusan exe tersebut dengan command rm.

#Poin e
```c
int main(int argc, char *argv[]) {
     if(argc != 2 || argv[1][1] != 'a' && argv[1][1] != 'b'){
        printf("Jalankan program dengan memilih paramter sebagai berikut:\n -a (./lukisan -a) atau -b (./lukisan -b)\n");
        printf("\n Berikut detail penjelesan program dengan parameter yang digunakan\n");
        printf("- Paramter -a :\n ketika program killer dijalankan, program utama akan langsung menghentikan semua operasinya\n");
        printf("\n- Paramter -b :\n ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete)\n");
        return 0;
    }

    pid_t pid, sid;
    
    pid = fork();

    if (pid < 0) {
      exit(EXIT_FAILURE);
    }

    if (pid > 0) {
      exit(EXIT_SUCCESS);
    }

    create_killer(getpid(), argv[1][1]);
```
Poin d dapat menyelesaikannya dengan cara sebagai berikut:
- Untuk menerima parameter ketika ./lukisan dijalankan, maka program utama (int main()) akan ditambahkan parameter int argc, char *argv[] sebagai tempat penyimpanan parameter yang akan digunakan.
- Untuk menghindari program yang lepas kendali, maka harus dilakukan pengkondisian bahwa user harus memasukan argumen yang bisa berupa -a atau -b.
- Untuk langsung membuat program killer ketika ./lukisan dijalankan, maka akan digunakan fungsi create_killer dengan parameter pid dan argumen.
- Pada MODE_A (-a), program utama akan langsung berhenti ketika program killer dijalankan. Untuk memenuhi kondisi tersebut, kita harus meng-kill semua process yang sedang berjalan. Oleh karena itu, ketika argumen/parameter "-a" dipassing ke fungsi create_killer(), fungsi tersebut akan membuat argumen "killall -9 lukisan" serta dengan execv killall tersebut.
- Pada MODE_B (-b), program akan berhenti, tetapi akan membiarkan proses berjalan samapai selesai (sampai menghapus folder). Untuk memenuhi kondisi tersebut, kita harus meng-kill parent processnya saja. Proses child yang telah di fork() akan tetap berjalan karena sudah di set sid meskipun parent processnya sudah dikill. Oleh karena itu, ketika argumen/parameter "-b" dipassing ke fungsi create_killer(), fungsi tersebut akan membuat argumen "kill PID" yang didapat dengan getpid() serta dengan execv kill tersebut.

Output soal2:
- Gambar yang terdonwload dalam sebuah folder dengan rincian ukuran gambar yang berbeda:
![Images_With_Size_Detail](/uploads/1593ac4b3b6367dae373a5967cf536a4/Images_With_Size_Detail.png)
- Catatan: Terdapat kasus dimana gambar yang telah didownload nama filenya lebih dari 5 detik. Hal ini dikarenakan kecepatan internet dan kecepatan mesin.

- Gambar di dalam folder yang telah di-zip:
![Zip_Images](/uploads/204ff2e36e185089c7684b7f19fcfe6f/Zip_Images.png)

- Jalannya program dengan parameter -a SEBELUM dijalankannya program killer:
![MODE_A_Before](/uploads/129a2fa2bf80654a1a47abb6af6f1f0e/MODE_A_Before.png)

- Jalannya program dengan parameter -a SETELAH dijalankannya program killer:
![MODE_A_After](/uploads/82bca95cf5033d681e15158e0e2985b8/MODE_A_After.png)

- Jalannya program dengan parameter -b SEBELUM dijalankannya program killer:
![MODE_B_Before](/uploads/d514bba1010ca62ff8bdd851802cf8b4/MODE_B_Before.png)

- Jalannya program dengan parameter -b SEBELUM dijalankannya program killer:
![MODE_B_After](/uploads/98ffbe5a05bc81ae8cc15aef0bf26990/MODE_B_After.png)

## Soal 3

Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. 
Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. 
Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. 
Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut hanya dengan 1 Program C bernama “filter.c”

a. Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.

b. Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.  

c. Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dengan waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.

d. Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/

Catatan:
- Format nama file yang akan diunduh dalam zip berupa [nama]_[tim]_[posisi]_[rating].jpg
- Tidak boleh menggunakan system()
- Tidak boleh memakai function C mkdir() ataupun rename().
- Gunakan exec() dan fork().
- Directory “.” dan “..” tidak termasuk yang akan dihapus.
- Untuk poin d DIWAJIBKAN membuat fungsi bernama buatTim(int, int, int), dengan input 3 value integer dengan urutan bek, gelandang, dan striker.

Penyelesaian dan penjelasan: 
```c
int main() {

    fileManage();

    removeFileNotContains("./players", "*ManUtd*");

    groupingPosition("./players");

    buatTim(4,3,3);

    return 0;
}
```
Untuk menyelesaikan soal tersebut dibagi menjadi 4 tahap, yaitu:
- Mendownload dan mengunzip file pemain dari internet
- Menghapus semua file yang tidak sesuai
- Mengelompokkan pemain berdasarkan posisinya
- Membuat tim berdasarkan formasi yang diinginkan

```c
void fileManage(){

    char file_url[] = "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download";
    char nama_file[] = "file.zip";

    child_id = fork();
    if(child_id == 0) {
        execlp("wget", "wget", "-O", nama_file, file_url, NULL);
    }

    while ((wait(&status)) > 0);
    child_id = fork();
    if(child_id == 0) {
        execlp("unzip", "unzip", nama_file, NULL);
    }

    while ((wait(&status)) > 0);
    child_id = fork();
    if(child_id == 0) {
        execlp("rm", "rm", nama_file, NULL);
    }

    printf("Done Donwloading and Unzipping file\n");
}
```

- Kita Download file player.zip dari gdrive dengan menggunakan wget pada exec (membuat child proses)
- Setelah child proses wget selesai, kita buat child proses lagi untuk meng unzip file.zip tersebut
- Lalu terakhir, setelah diunzip, kita hapus file player.zip nya menggunakan exec rm

```c
void removeFileNotContains(char pt[], char st[]){
    while ((wait(&status)) > 0);
    child_id = fork();
    if(child_id == 0) {
        execlp("find", "find", pt, "-type", "f", "!", "-name", st, "-delete", NULL);
    }

    printf("Done filtering Man United\n");
}
```

Langkah selanjutnya adalah kita membuat proses baru untuk menjalankan fungsi find, mencari semua file yang tidak mengandung "ManUtd" lalu mendelete nya.

```c
void mkdirGroup(){

    child_id = fork();
    if(child_id == 0) {
        execlp("mkdir", "mkdir", "./players/penyerang", NULL);
    }
    child_id = fork();
    if(child_id == 0) {
        execlp("mkdir", "mkdir", "./players/bek", NULL);
    }
    child_id = fork();
    if(child_id == 0) {
        execlp("mkdir", "mkdir", "./players/gelandang", NULL);
    }
    child_id = fork();
    if(child_id == 0) {
        execlp("mkdir", "mkdir", "./players/kiper", NULL);
    }

    printf("Done Create folder\n");

}

void groupingPosition(char pt[]){

    mkdirGroup();
    while ((wait(&status)) > 0);
    child_id = fork();
    if(child_id == 0){
        execlp("find", "find", pt, "-iname", "*Penyerang*", "-exec", "mv", "-n","{}", "./players/penyerang/" , ";", NULL);
    }
    child_id = fork();
    if(child_id == 0){
        execlp("find", "find", pt, "-iname", "*Bek*", "-exec", "mv", "-n", "{}", "./players/bek/" , ";", NULL);
    }

    child_id = fork();
    if(child_id == 0){
        execlp("find", "find", pt, "-iname", "*Gelandang*", "-exec", "mv", "-n", "{}", "./players/gelandang/" , ";", NULL);
    }

    child_id = fork();
    if(child_id == 0){
        execlp("find", "find", pt, "-iname", "*Kiper*", "-exec", "mv", "-n", "{}", "./players/kiper/" , ";", NULL);
    }

    while ((wait(&status)) > 0);
    printf("Done Grouping\n");
}
```

- Kita buat folder untuk masing masing posisi dengan menggunakan mkdirGroup(), setiap proses mkdir akan dijalankan pada proses yang berbeda secara bersamaan
- Setelah menunggu proses mkdirGroup() selesai. setiap file akan dipindahkan ke foldernya masing masing sesuai dengan posisi yang terletak pada nama filenya.
- Setiap proses pemindahan untuk masing masing posisi akan dilakukan pada proses yang berbeda dan dilakukan bersamaan. seperti yang terlihat tidak ada while(wait) antar setiap proses


```c
void buatTim(int bek, int gelandang, int penyerang){

    child_id = fork();
    if(child_id == 0){
        char cmd[100];
        snprintf(cmd, sizeof(cmd), "ls ./players/kiper | sort -t _ -nk4 | tail -n 1 >> ~/Formasi_%d_%d_%d.txt", bek, gelandang, penyerang);
        execlp("sh", "sh", "-c", cmd, NULL);
    }

    while ((wait(&status)) > 0);
    child_id = fork();
    if(child_id == 0){
        char cmd[100];
        snprintf(cmd, sizeof(cmd), "ls ./players/bek | sort -t _ -nk4 | tail -n %d >> ~/Formasi_%d_%d_%d.txt",bek, bek, gelandang, penyerang);
        execlp("sh", "sh", "-c", cmd, NULL);
    }

    while ((wait(&status)) > 0);
    child_id = fork();
    if(child_id == 0){
        char cmd[100];
        snprintf(cmd, sizeof(cmd), "ls ./players/gelandang | sort -t _ -nk4 | tail -n %d >> ~/Formasi_%d_%d_%d.txt",gelandang, bek, gelandang, penyerang);
        execlp("sh", "sh", "-c", cmd, NULL);
    }

    while ((wait(&status)) > 0);
    child_id = fork();
    if(child_id == 0){
        char cmd[100];
        snprintf(cmd, sizeof(cmd), "ls ./players/penyerang | sort -t _ -nk4 | tail -n %d >> ~/Formasi_%d_%d_%d.txt",penyerang, bek, gelandang, penyerang);
        execlp("sh", "sh", "-c", cmd, NULL);
    }
    
    while ((wait(&status)) > 0);
    printf("Done Make a Formation\n");
}
```

- Lalu untuk buatTim(), fungsi ini menerima 3 parameter yaitu jumlah bek, gelandang, dan penyerang
- Proses pemilihan pemain untuk setiap posisi sama, yaitu :
    - Mendapatkan list nama file/pemain dari setiap folder posisi yang sudah dikelompokkan
    - Mengurutkan hasil list berdasarkan rating pemain secara ascending
    - Mengambil n pemain dengan rating tertinggi menggunakan tail berdasarkan jumlah pemain pada posisi tersebut yang dimasukkan sebagai parameter (Kiper pasti 1)
    - Memasukkan list pemain yang terpilih ke file Formasi-[bek]_[gelandang]_[penyerang].txt
- Setiap proses dilakukan secara seri/sequential agar hasil pada file Formasi.txt tidak saling overlapping

Output buatTim(4,3,3):
```
DeGea_ManUtd_Kiper_87.png
Maguire_ManUtd_Bek_80.png
Shaw_ManUtd_Bek_82.png
Martinez_ManUtd_Bek_83.png
Varane_ManUtd_Bek_85.png
Eriksen_ManUtd_Gelandang_83.png
Fernandes_ManUtd_Gelandang_86.png
Casemiro_ManUtd_Gelandang_89.png
Antony_ManUtd_Penyerang_81.png
Sancho_ManUtd_Penyerang_83.png
Rashford_ManUtd_Penyerang_84.png
```


## Soal 4 (revisi : percabangan pertama)

- revisi pada bagian dibawah ini : 
```c
if (argc < 5 || argc > 5) {
    printf("invalid argument: %s <hour> <minute> <second> <script-path>\n", argv[0]);
    return 1;
}//revisi pada bagian ini
```

Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C karena baru dipelajari olehnya.

Untuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:

- Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.

- Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.

- Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.

- Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.

- Bonus poin apabila CPU state minimum.


pertama kita perlu mengecek format dari input argument, dimana formatnya adalah :  /program \* 44 5 /home/Banabil/programcron.sh

kita dapat menggunakan 
```c

if (argc < 5 || argc > 5) {
    printf("invalid argument: %s <hour> <minute> <second> <script-path>\n", argv[0]);
    return 1;
}//revisi pada bagian ini

if (strcmp(argv[1], "*") == 0) {
    hour = -1;
} else {
    hour = atoi(argv[1]);
    if (hour < 0 || hour > 23) {
        printf("Invalid hour value\n");
        return 1;
    }
}

if (strcmp(argv[2], "*") == 0) {
    minute = -1;
} else {
    minute = atoi(argv[2]);
    if (minute < 0 || minute > 59) {
        printf("Invalid minute value\n");
        return 1;
    }
}

if (strcmp(argv[3], "*") == 0) {
    second = -1;
} else {
    second = atoi(argv[3]);
    if (second < 0 || second > 59) {
        printf("Invalid second value\n");
        return 1;
    }
}
```

- ini adalah untuk specify bahwa command yang dijalankan adalah shell command
```c
    // Define the arguments for the program to be executed
    char *args[] = {"/bin/bash", argv[4], NULL};
```


- lalu untuk mengecek jam/waktu sekarang menggunakan library time.h

```c
    // getting time
    time_t now = time(NULL);
    struct tm *timeStruct = localtime(&now);
    int hournow, minnow, secnow;
    bool first = true;

    hournow = timeStruct->tm_hour;
    minnow = timeStruct->tm_min;
    secnow = timeStruct->tm_sec;
```


- kemudian masuk ke pengecekan waktu untuk **sleep pertama**, apabila waktu sekarang memenuhi syarat untuk dijalankan pada saat itu juga maka program akan berjalan langsung saat itu juga, setelah itu baru menjalani sleep
- dapat dilihat apabila * * * maka sleeping = 0, begitu juga untuk hours dan minute
- beberapa hal yang tricky adalah apabila jam sekarang adalah 13 40 50 tetapi jam yang diinginkan untuk cron adalah lebih kecil dari jam tersebut, sehingga terdapat penambahan jam atau bahkan hari apabila hal tersebut terjadi. 

```c
    int sleeping = 0;

    if (hour == -1)
    {
        if (minute == -1)
        {
            if (second >= 0)
            {

                if (second < secnow)
                {
                    sleeping = (second + 60) - secnow;
                }

                else
                {
                    sleeping = second - secnow;
                }
            }
            else
            {
                sleeping = 0;
            }
        }
        else
        {
            sleeping = (((minute * 60) + second) - ((minnow * 60) + secnow));
            if (minute < minnow)
            {
                sleeping += 3600;
            }
            else if (minute == minnow)
            {
                sleeping = 0;
            }
        }
    }

    else
    {
        sleeping = (((hour * 3600) + (minute * 60) + second) - ((hournow * 3600) + (minnow * 60) + secnow));
        if (hour < hournow)
        {
            sleeping += 86400;
        }
        else if (hour == hournow)
        {
            sleeping = 0;
        }
    }
```

```c
    printf(" current hour: %d %d %d \n", hournow, minnow, secnow);
    printf("cronjob hour : %d %d %d \n", hour, minute, second);
    printf("%d\n", sleeping);

``` 
- ini hanyalah print untuk mengecek jam sekarang dan jam input argument



- berikut ini adalah code untuk bagian forking (running in background)
```c
// Fork a new process
    pid_t pid = fork();

    if (pid == -1)
    {
        printf("Failed to fork process\n");
        return 1;
    }
    else if (pid == 0)
    {
        // In child process
        // Run the specified program at the specified interval

        while (1)
        {
            
            int slept = 0;
            if(hour == -1 && minute == -1 && second == -1){
                slept = 1;
            }
            
            if (hour != -1)
            {
                slept += 86400;
            }
            else if (minute != -1)
            {
                slept += 3600;
            }
            else if (second != -1)
            {
                slept += 60;
            }

            if (first == true)
            {
                sleep(sleeping);
                first = false;

                if (hour ==-1)
                {

                    if (minute == -1)
                    {

                        if (second == -1)
                        {
                            slept = 1;
                        }
                    }
                    else
                    {
                        if (second == -1)
                        {
                            slept -= secnow;
                        }
                    }
                }
                else
                {
                    if (minute == -1 && second == -1)
                    {
                        slept -= ((minnow * 60) + secnow);
                    }
                    else if (minute == -1 && second >= 0)
                    {
                        slept -= (minnow * 60);
                    }
                    else if(minute >= 0 && second == -1)
                    {
                        slept -= secnow;
                    }
                }
            }

            pid_t exec_pid = fork();
            if (exec_pid == -1)
            {
                printf("Failed to fork process\n");
            }

            else if (exec_pid == 0)
            {
                // Child process for executing the script
                printf("Current time %d %d %d\n", hournow, minnow, secnow);
                execvp(args[0], args);
            }
            else
            {
                // Parent process waits for the script to finish execution
                wait(NULL);
            }

            // print slept interval
            printf("slept time: %d\n", slept);
            sleep(slept);
        }
    }
    else
    {
        //ini parent yang di exit, supaya program berjalan di background
        printf("Timer process is running in the background with PID: %d\n", pid);
        return 0;
    }

```

```c
    pid_t pid = fork();

    if (pid == -1)
    {
        printf("Failed to fork process\n");
        return 1;
    }
    else if (pid == 0)
    { ...... }
      else
    {
        //ini parent yang di exit, supaya program berjalan di background
        printf("Timer process is running in the background with PID: %d\n", pid);
        return 0;
    }
```
- disini adalah forking pertamanya, untuk pid -1 adalah error, dan pid = adalah child process 
- sedangkan pid positifnya adalah parent process yang di return/exit langsung supaya program berjalan di background


```c
// In child process
        // Run the specified program at the specified interval

        while (1)
        {
            
            int slept = 0;
            if(hour == -1 && minute == -1 && second == -1){
                slept = 1;
            }
            
            if (hour != -1)
            {
                slept += 86400;
            }
            else if (minute != -1)
            {
                slept += 3600;
            }
            else if (second != -1)
            {
                slept += 60;
            }

            if (first == true)
            {
                sleep(sleeping);
                first = false;

                if (hour ==-1)
                {

                    if (minute == -1)
                    {

                        if (second == -1)
                        {
                            slept = 1;
                        }
                    }
                    else
                    {
                        if (second == -1)
                        {
                            slept -= secnow;
                        }
                    }
                }
                else
                {
                    if (minute == -1 && second == -1)
                    {
                        slept -= ((minnow * 60) + secnow);
                    }
                    else if (minute == -1 && second >= 0)
                    {
                        slept -= (minnow * 60);
                    }
                    else if(minute >= 0 && second == -1)
                    {
                        slept -= secnow;
                    }
                }
            }

            pid_t exec_pid = fork();
            if (exec_pid == -1)
            {
                printf("Failed to fork process\n");
            }

            else if (exec_pid == 0)
            {
                // Child process for executing the script
                printf("Current time %d %d %d\n", hournow, minnow, secnow);
                execvp(args[0], args);
            }
            else
            {
                // Parent process waits for the script to finish execution
                wait(NULL);
            }

            // print slept interval
            printf("slept time: %d\n", slept);
            sleep(slept);
        }
```

- didalam child pertama ini ada pengecekan/perhitungan waktu lagi yang ditampung di **slept**, slept ini digunakan untuk menghitung sleep yang kedua kali dan seterusnya. karena waktu sleep yang pertama dan yang seterusnya itu berbeda karena faktor diawal tadi (current hour lebih dari desired hour)
- disini slept juga seperti itu, tetapi lebih mudah karena apabila * * * maka sleep = 1, lalu apabila hanya ada detik maka ditambah 1 menit, menit ditambahkan 1 jam, dan jam ditambahkan 1 hari. 

```c
            if(hour == -1 && minute == -1 && second == -1){
                slept = 1;
            }
            
            if (hour != -1)
            {
                slept += 86400;
            }
            else if (minute != -1)
            {
                slept += 3600;
            }
            else if (second != -1)
            {
                slept += 60;
            }
```
- tetapi apabila program berjalan di pukul (misal) 17 : 15 : 16, untuk * 15 *, maka program akan berjalan di detik 15 terus menerus, maka hal ini harus diatasi untuk membuat waktu program berjalan berikutnya pada detik 0 (misal berjalan lagi pada pukul 18 : 15 : 0).

```c
if (first == true)
            {
                sleep(sleeping);
                first = false;

                if (hour ==-1)
                {

                    if (minute == -1)
                    {

                        if (second == -1)
                        {
                            slept = 1;
                        }
                    }
                    else
                    {
                        if (second == -1)
                        {
                            slept -= secnow;
                        }
                    }
                }
                else
                {
                    if (minute == -1 && second == -1)
                    {
                        slept -= ((minnow * 60) + secnow);
                    }
                    else if (minute == -1 && second >= 0)
                    {
                        slept -= (minnow * 60);
                    }
                    else if(minute >= 0 && second == -1)
                    {
                        slept -= secnow;
                    }
                }
            }
```

- pada kode diataslah dilakukan pengurangan waktu sleep untuk menyesuakan hour/menit tersebut 

- sisanya adalah menjalankan script yang diberikan pada input tersebut 

```c
pid_t exec_pid = fork();
            if (exec_pid == -1)
            {
                printf("Failed to fork process\n");
            }

            else if (exec_pid == 0)
            {
                // Child process for executing the script
                execvp(args[0], args);
            }
            else
            {
                // Parent process waits for the script to finish execution
                wait(NULL);
            }

            // print slept interval
            printf("slept time: %d\n", slept);
            sleep(slept);
```

- diatas digunakan kembali fork untuk executing, dan digunakan execvp untuk menjalankan args[0] yaitu executable hasil compile mainan.c