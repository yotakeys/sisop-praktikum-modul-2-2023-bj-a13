#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#include <string.h>
#include <time.h>

int main(){
  pid_t child_id;
  int status;

  //download in link
  child_id = fork();
  if(child_id < 0){
    exit(EXIT_FAILURE);
  }else if(child_id == 0){ //child process
    char *argv[] = {"wget", "-O", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "&", NULL};
    execv("/usr/bin/wget", argv);
    return 0;
  }
  
  while((wait(&status)) > 0);

  //unzip the zip file
  child_id = fork();
  if(child_id < 0){
    exit(EXIT_FAILURE);
  }else if(child_id == 0){ //child process
    char *argv[] = {"unzip", "binatang.zip", NULL};
    execv("/usr/bin/unzip", argv);
    return 0;
  }
  
  while((wait(&status)) > 0);

  //take random filename
  child_id = fork();
  if(child_id < 0){
    exit(EXIT_FAILURE);
  }else if(child_id == 0){ //child process
    struct dirent *entry;
    DIR *dir = opendir(".");
    if(dir == NULL){
        printf("Error open dir");
        exit(EXIT_FAILURE);
    }

    //count number of file
    int count = 0;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            count++;
        }
    }
    
    //allocate memory for name of file
    char **files = malloc(count * sizeof(char *));
    if (files == NULL) {
        printf("Error malloc");
        exit(EXIT_FAILURE);
    }

    //return the pointer back to first file
    rewinddir(dir);
    
    //take name of file name
    int i = 0;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG && strstr(entry->d_name, ".jpg")) { 
            files[i] = entry->d_name;
            i++;
        }
    }
    //close directory
    closedir(dir);

    //random number
    srand(time(NULL));
    int random = rand() % i;
    
    //select file
    char filename[50];
    strcpy(filename, files[random]);
    printf("==========================================================\n");
    printf("MELAKUKAN SHIFT PENJAGAAN PADA : %s\n", filename);
    printf("==========================================================\n");
    free(files);
    
    return 0;
  }

  while((wait(&status)) > 0);

  //create directory
  system("mkdir HewanDarat HewanAir HewanAmphibi");
  //move file
  system("mv *amphibi.jpg HewanAmphibi");
  system("mv *darat.jpg HewanDarat");
  system("mv *air.jpg HewanAir");

  while((wait(&status)) > 0);

  //zip the directory
  child_id = fork();
  if(child_id < 0){
    exit(EXIT_FAILURE);
  }else if(child_id == 0){ //child process
    char *argv[] = {"zip", "-r", "HewanAir.zip", "HewanAir", NULL};
    execv("/usr/bin/zip", argv);
    return 0;
  }
  child_id = fork();
  if(child_id < 0){
    exit(EXIT_FAILURE);
  }else if(child_id == 0){ //child process
    char *argv[] = {"zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
    execv("/usr/bin/zip", argv);
    return 0;
  }
  child_id = fork();
  if(child_id < 0){
    exit(EXIT_FAILURE);
  }else if(child_id == 0){ //child process
    char *argv[] = {"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
    execv("/usr/bin/zip", argv);
    return 0;
  }
  
  //delete all directory just leaving zip file
  while((wait(&status)) > 0);
  system("rm -rf HewanAir HewanDarat HewanAmphibi binatang.zip");
  return 0;
}