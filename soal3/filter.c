// Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. 
// Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. 
// Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. 
// Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut hanya dengan 1 Program C bernama “filter.c”
// a. Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.
// b. Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.  
// c. Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dengan waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.
// d. Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/
// Catatan:
// - Format nama file yang akan diunduh dalam zip berupa [nama]_[tim]_[posisi]_[rating].jpg
// - Tidak boleh menggunakan system()
// - Tidak boleh memakai function C mkdir() ataupun rename().
// - Gunakan exec() dan fork().
// - Directory “.” dan “..” tidak termasuk yang akan dihapus.
// - Untuk poin d DIWAJIBKAN membuat fungsi bernama buatTim(int, int, int), dengan input 3 value integer dengan urutan bek, gelandang, dan striker.

#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <stdio.h>
#include <dirent.h>

pid_t child_id;
int status;

void fileManage(){

    char file_url[] = "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download";
    char nama_file[] = "file.zip";

    child_id = fork();
    if(child_id == 0) {
        execlp("wget", "wget", "-O", nama_file, file_url, NULL);
    }

    while ((wait(&status)) > 0);
    child_id = fork();
    if(child_id == 0) {
        execlp("unzip", "unzip", nama_file, NULL);
    }

    while ((wait(&status)) > 0);
    child_id = fork();
    if(child_id == 0) {
        execlp("rm", "rm", nama_file, NULL);
    }

    printf("Done Donwloading and Unzipping file\n");
}

void mkdirGroup(){

    child_id = fork();
    if(child_id == 0) {
        execlp("mkdir", "mkdir", "./players/penyerang", NULL);
    }
    child_id = fork();
    if(child_id == 0) {
        execlp("mkdir", "mkdir", "./players/bek", NULL);
    }
    child_id = fork();
    if(child_id == 0) {
        execlp("mkdir", "mkdir", "./players/gelandang", NULL);
    }
    child_id = fork();
    if(child_id == 0) {
        execlp("mkdir", "mkdir", "./players/kiper", NULL);
    }

    printf("Done Create folder\n");

}

void removeFileNotContains(char pt[], char st[]){
    while ((wait(&status)) > 0);
    child_id = fork();
    if(child_id == 0) {
        execlp("find", "find", pt, "-type", "f", "!", "-name", st, "-delete", NULL);
    }

    printf("Done filtering Man United\n");
}

void groupingPosition(char pt[]){

    mkdirGroup();
    while ((wait(&status)) > 0);
    child_id = fork();
    if(child_id == 0){
        execlp("find", "find", pt, "-iname", "*Penyerang*", "-exec", "mv", "-n","{}", "./players/penyerang/" , ";", NULL);
    }
    child_id = fork();
    if(child_id == 0){
        execlp("find", "find", pt, "-iname", "*Bek*", "-exec", "mv", "-n", "{}", "./players/bek/" , ";", NULL);
    }

    child_id = fork();
    if(child_id == 0){
        execlp("find", "find", pt, "-iname", "*Gelandang*", "-exec", "mv", "-n", "{}", "./players/gelandang/" , ";", NULL);
    }

    child_id = fork();
    if(child_id == 0){
        execlp("find", "find", pt, "-iname", "*Kiper*", "-exec", "mv", "-n", "{}", "./players/kiper/" , ";", NULL);
    }

    while ((wait(&status)) > 0);
    printf("Done Grouping\n");
}

void buatTim(int bek, int gelandang, int penyerang){

    child_id = fork();
    if(child_id == 0){
        char cmd[100];
        snprintf(cmd, sizeof(cmd), "ls ./players/kiper | sort -t _ -nk4 | tail -n 1 >> ~/Formasi_%d_%d_%d.txt", bek, gelandang, penyerang);
        execlp("sh", "sh", "-c", cmd, NULL);
    }

    while ((wait(&status)) > 0);
    child_id = fork();
    if(child_id == 0){
        char cmd[100];
        snprintf(cmd, sizeof(cmd), "ls ./players/bek | sort -t _ -nk4 | tail -n %d >> ~/Formasi_%d_%d_%d.txt",bek, bek, gelandang, penyerang);
        execlp("sh", "sh", "-c", cmd, NULL);
    }

    while ((wait(&status)) > 0);
    child_id = fork();
    if(child_id == 0){
        char cmd[100];
        snprintf(cmd, sizeof(cmd), "ls ./players/gelandang | sort -t _ -nk4 | tail -n %d >> ~/Formasi_%d_%d_%d.txt",gelandang, bek, gelandang, penyerang);
        execlp("sh", "sh", "-c", cmd, NULL);
    }

    while ((wait(&status)) > 0);
    child_id = fork();
    if(child_id == 0){
        char cmd[100];
        snprintf(cmd, sizeof(cmd), "ls ./players/penyerang | sort -t _ -nk4 | tail -n %d >> ~/Formasi_%d_%d_%d.txt",penyerang, bek, gelandang, penyerang);
        execlp("sh", "sh", "-c", cmd, NULL);
    }
    
    while ((wait(&status)) > 0);
    printf("Done Make a Formation\n");
}

int main() {

    fileManage();

    removeFileNotContains("./players", "*ManUtd*");

    groupingPosition("./players");

    buatTim(4,3,3);

    return 0;
}

// How to run:
// - cd to soal3/
// - gcc filter.c -o filter
// - ./filter